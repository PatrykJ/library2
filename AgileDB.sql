CREATE DATABASE IF NOT EXISTS AgileDB;

CREATE TABLE IF NOT EXISTS students	(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    UserName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(10) NOT NULL,
    firstName		VARCHAR(15) NOT NULL,
    lastName		VARCHAR(15) NOT NULL,
    MobileNum		INTEGER NOT NULL);
    
CREATE TABLE IF NOT EXISTS admin		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	UserName		VARCHAR(30) NOT NULL,
    NickName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(10) NOT NULL,
    firstName		VARCHAR(15) NOT NULL,
    lastName		VARCHAR(15) NOT NULL,
    MobileNum		INTEGER NOT NULL);

CREATE TABLE IF NOT EXISTS book		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    Title			VARCHAR(30) NOT NULL,
    Author			VARCHAR(30) NOT NULL,
    Genre			VARCHAR(20) NOT NULL,
    Publisher		VARCHAR(30) NOT NULL,
    EditionNumber	Varchar(20) NOT NULL,
    ISBN			VARCHAR(40) NOT NULL,
    Stock			VARCHAR(3) NOT NULL);

CREATE TABLE IF NOT EXISTS journals		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    Title			VARCHAR(30) NOT NULL,
    Author			VARCHAR(30) NOT NULL,
    Genre			VARCHAR(20) NOT NULL,
    Publisher		VARCHAR(30) NOT NULL,
    EditionNumber	Varchar(20) NOT NULL,
    Stock			VARCHAR(3) NOT NULL);

INSERT INTO book VALUES(null, 'PHP, MySQL, JavaScript & HTML5 for DUMMIES', 'Steven Suehring', 'Software', 'John Willey & Sons Inc.', '1st', '978-1-118-21370-4', 'Yes');    
INSERT INTO book VALUES(null, 'Dynamics of WORDPERFECT', 'Dow Jones-Irwin ', 'Software', 'McGraw-Hill Professional Publishing', '1st', '978-0-870-94655-4', 'No');    
INSERT INTO journals VALUES(null, 'PC Magazine', 'Dan Costa ', 'PC', 'Ziff Davis INC.', 'January 2017', 'Yes');    
INSERT INTO journals VALUES(null, 'PC Magazine', 'Dan Costa', 'PC', 'Ziff Davis INC.', 'Feburary 2017', 'Yes');    
    
INSERT INTO students VALUES (null, 'oskar', 'klosowski69', 'Oskar', 'Klosowski', 070088077);
INSERT INTO students VALUES (null, 'homer21', 'simpson34', 'Homer', 'Simpson', 0831245858);
INSERT INTO students VALUES (null, 'long21', 'strong18', 'Anna', 'Strong', 0875221542);
INSERT INTO admin VALUES(null, 'adminX123', 'TheLegend27', 'IamAdmin23', 'Owner', 'OfTheSystem', 0878221312);    