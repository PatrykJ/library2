package library2;



public class userBean {
	
	static String id;
	static String UserName;
	static String name; 
	static String surname;
	static String email;
	static String Mobile;
	static String pwd; 
	
	public userBean(String id, String UserName, String name, String surname, String email, String Mobile, String pwd)
	{
		this.id = id; 
		this.UserName = UserName; 
		this.name = name; 
		this.surname= surname; 
		this.email = email;
		this.Mobile = Mobile; 
		this.pwd =pwd;
	}
	public static String getId() {
		return id;
	}
	public static void setId(String userId) {
		id = userId;
	}
	public static String getUserName() {
		return UserName;
	}
	public static void setUserName(String userName) {
		UserName = userName;
	}
	public static  String getName() {
		return name;
	}
	public static void setName(String firstname) {
		name = firstname;
	}
	public static String getSurname() {
		return surname;
	}
	public static void setSurname(String familyname) {
		surname = familyname;
	}
	public static String getEmail() {
		return email;
	}
	public static void setEmail(String email) {
		userBean.email = email;
	}
	public static String getMobile() {
		return Mobile;
	}
	public static void setMobile(String mobile) {
		Mobile = mobile;
	}
	public String getPwd() {
		return pwd;
	}
	public static void setPwd(String password) {
		pwd = password;
	}
}
