package library2;



import java.sql.*;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import LibraryException.LibExceptionHandler;
import Model.Overdue;
import UserDAO.DBConn;
import UserDAO.DBSelection;
import View.Login;
import View.Register;
import View.UserHome;

public class User extends JFrame {
	
	public User() {
	}

	public static boolean login(String login, String email, String pass) throws LibExceptionHandler {
		Connection conn = DBConn.getConnection(DBSelection.databaseName);
		System.out.println("Login"+DBSelection.databaseName);
		System.out.println(conn);
		boolean connection = false;
		try {

			
			String cmd = "SELECT * FROM students where UserName='" + login + "' and pass='" + pass + "';";
			PreparedStatement ps = conn.prepareStatement(cmd);
			ResultSet rs = ps.executeQuery(cmd);
			int count = 0;

			while (rs.next()) {
				count++;
				System.out.println(rs.getString(1));
				userBean.setId(rs.getString(1));
				userBean.setUserName(rs.getString(2));
				userBean.setPwd(rs.getString(3));
				userBean.setName(rs.getString(4));
				userBean.setSurname(rs.getString(5));
				userBean.setEmail(rs.getString(6));
				userBean.setMobile(rs.getString(7));
			}

			if (count == 1) {
				// JOptionPane.showMessageDialog(null, "User, Found Access
				// Granted!");
				new UserHome().setVisible();
				new Login().setVisible(false);
				connection = true;
			} else if (count > 1) {
				JOptionPane.showMessageDialog(null, "Duplicate User, Access Denied!");
			} else {
				JOptionPane.showMessageDialog(null, "user doesn't exist. ");
			}

		} catch (Exception ex) {
			System.out.println("exception 2 ");
		}
		return connection;

	}

	public static boolean Register(String username, String password, String email, String name, String surname,
			String iD) throws SQLException 
	{
		boolean connection = false;
		try 
		{
			String url = "jdbc:mysql://localhost:3306/AgileDB";
			Connection databaseConnection = DriverManager.getConnection(url, "karl", "20071985Kh");
			Statement stmt = (Statement) databaseConnection.createStatement();

			try 
			{
				String cmd = ("INSERT INTO students VALUES(null, '" + username + "','" + password + "','" + name
									+ "','" + surname + "','" + email + "','" + iD + "');");
				PreparedStatement ps = databaseConnection.prepareStatement(cmd);
				stmt.executeUpdate(cmd);
				connection = true;
				JOptionPane.showMessageDialog(null, "Account Created");
				

				Register.getMainFrame().dispose();
				Login.getMainFrame().setVisible(true);
				
			}
		catch(SQLException sqle)
		{
			System.err.println("Error with  insert:\n" + sqle.toString());
		}

	}
	catch(Exception ex)
	{
		System.out.println("exception 2 " + ex);
	}
	return connection;
}

	public static boolean passwordCheck(String username, String password, String SecondPassowrd, String email,
			String name, String surname, String iD) {
		boolean flag = false;
		try {
			if (password.equals(SecondPassowrd)) {
				Register(username, password, email, name, surname, iD);
				flag = true;
			}
		} catch (Exception ex) {
			System.out.println("Error occured: " + ex);
		}
		return flag;
	}

	public static boolean existingUser(String username, String password, String SecondPassowrd, String email,
			String name, String surname, String iD) {
		boolean flag = false;
		try {
			String url = "jdbc:mysql://localhost:3306/AgileDB";
			Connection databaseConnection = DriverManager.getConnection(url, "karl", "20071985Kh");
			Statement stmt = (Statement) databaseConnection.createStatement();

			String cmd = ("Select UserName from students where UserName = '" + username + "';");

			PreparedStatement ps = databaseConnection.prepareStatement(cmd);
			stmt.executeQuery(cmd);
			ResultSet rs = stmt.executeQuery(cmd);
			if (rs.next()) {
				JOptionPane.showMessageDialog(null, "User already in the system");
			}

			else {
				passwordCheck(username, password, SecondPassowrd, email, name, surname, iD);
				flag = true;
			}
		} catch (SQLException sqle) {
			System.err.println("Error with  insert:\n" + sqle.toString());
		}
		return flag;
	}
	
	public static void changePWD(String pwd) {
		try{
		String url = "jdbc:mysql://localhost:3306/AgileDB";
		Connection databaseConnection = DriverManager.getConnection(url, "karl", "20071985Kh");
		Statement stmt = (Statement) databaseConnection.createStatement();
		
		try{
			
			String cmd = "UPDATE student SET pass = '"+ pwd + "' WHERE id= '" +userBean.getId()+ "';";
			PreparedStatement ps = databaseConnection.prepareStatement(cmd);
			ResultSet rs = stmt.executeQuery(cmd);
			
		}catch(Exception ex)
		{
			System.out.print("Error with statement: "+ex);
		}
		
		}catch (SQLException se)
		{
			System.out.print("Error: "+se);
		  }
		
	}
	
	public static void changeDetails(String name, String surname, String phone, int id) {
		
		try{
			Connection conn = DBConn.getConnection(DBSelection.databaseName);
			System.out.println(conn);
			Statement stmt = (Statement) conn.createStatement();
			
			String cmd =( "UPDATE student SET firstName = '"+ name + "', lastName= '" + surname + "' , MobileNum = '" + phone + "' WHERE id= '" +userBean.getId()+ "';");
			PreparedStatement ps = conn.prepareStatement(cmd);
			//ResultSet rs = stmt.executeQuery(cmd);
			System.out.println(cmd);
			stmt.executeUpdate(cmd);
		}catch(Exception xx){
			System.out.println("Error: " + xx);
		}
		
	}
	
	public static boolean passwordCheck( String password, String SecondPassowrd) {
		boolean flag = false;
		try {
			if (password.equals(SecondPassowrd)) {
				changePWD(password);
			}
		} catch (Exception ex) {
			System.out.println("Error occured: " + ex);
		}
		return flag;
	}

	public static List<Overdue> getStudentOverDueDocuments() throws SQLException {
		List<Overdue> overDueListStudent = new DBConn().getAllOverdueStudent();
		
		return overDueListStudent;
	}
	
	
}
