package Tests;

import org.junit.Test;

import Controller.bookController;
import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;
import junit.framework.TestCase;

public class bookControllerTest extends TestCase {

	// Test No: 1
	// Input: empty textfield
	// Expected output(s): error message("one or more text is missing")

	@Test
	public void testAddBook1() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("", "", "", "", "", "", "");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}

	public void testAddBook2() {
		// bookController myBControl = new bookController();
		DBConn myDB = new DBConn();
		boolean res = myDB.addBook("antigony", "bbb", "aaa", "aaa", "aaa", "aa", "aa");
		System.out.println(res);
	}

	// @Test
	// public void testUpdateBook() {
	// fail("Not yet implemented");
	// }

	// Test No: 3
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook3() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "b", "c", "d", "e", "f", "");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}

	// Test No: 4
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook4() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "b", "c", "d", "e", "", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}

	// Test No: 5
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook5() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "b", "c", "d", "", "e", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}

	// Test No: 6
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook6() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "b", "c", "", "d", "e", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}

	// Test No: 7
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook7() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "b", "", "c", "d", "e", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}
	
	// Test No: 8
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook8() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("a", "", "b", "c", "d", "e", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}
	
	// Test No: 9
	// Input: empty textfield
	// Expected output(s): error message("one or more test is missing")

	public void testAddBook9() {
		bookController myBControl = new bookController();
		try {
			myBControl.addBook("", "a", "b", "c", "d", "e", "f");
			fail("Not yet implemented");
		} catch (LibExceptionHandler e) {
			assertEquals("one or more text is missing", e.getMessage());
		}
	}	

}
