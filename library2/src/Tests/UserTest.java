package Tests;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import LibraryException.LibExceptionHandler;
import View.CalcFine;
import View.Overdue_BooksGUI;
import junit.framework.TestCase;
import library2.User;

public class UserTest extends TestCase{
	// Test Number: 1
	// Test Objective: Check if the user is in the system already
	// Input(s): right username & password
	// Expected Output(s): Exception "Should not reach here ... no exception

	@Test
	public void test001() throws LibExceptionHandler {
		User test1 = new User();

		assertEquals(true,test1.login("homer21", "diajef@gmail.com", "simpson34"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 2
	// Test Objective: Check if the user is in the system already
	// Input(s): Incorrect username & password
	// Expected Output(s): Exception "Should not reach here ... no exception
	
	@Test
	public void test002() throws LibExceptionHandler {
		User test2 = new User();

		assertEquals(false,test2.login("oskar", "diajef@gmail.com", "paddss"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 3
	// Test Objective: Check if the usename has been taken
	// Input(s): unregistered username & password
	// Expected Output(s): Exception "Should not reach here ... no exception	
	
	@Test
	public void test003() throws LibExceptionHandler, SQLException {
		User test3 = new User();

		assertEquals(true,User.existingUser("zxxcxz", "passs", "Osksar", "Klossowski", "fNasme", "fNasme", "dcvefr"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 4
	// Test Objective: Check if the usename has been taken
	// Input(s): registered username & password
	// Expected Output(s): Exception "Should not reach here ... no exception	
	
	@Test
	public void test004() throws LibExceptionHandler, SQLException {
		User test4 = new User();

		assertEquals(true,User.passwordCheck("oskar", "pass", "pass", "Klossowski", "fNasme", "fNasme", "dcvefr"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 5
	// Test Objective: Check if the password and confirm password are the same
	// Input(s): registered username & password
	// Expected Output(s): Exception "Should not reach here ... no exception	
	
	@Test
	public void test005() throws LibExceptionHandler, SQLException {
		User test5 = new User();

		assertEquals(false,User.passwordCheck("oskar", "pass", "Osksar", "Klossowski", "fNasme", "fNasme", "dcvefr"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 6
	// Test Objective: Check if the password and confirm password are the same
	// Input(s): registered username & password
	// Expected Output(s): Exception "Should not reach here ... no exception	
	
	@Test
	public void test006() throws LibExceptionHandler, SQLException {
		User test6 = new User();

		assertEquals(false,User.existingUser("oskar", "pass", "Osksar", "Klossowski", "fNasme", "fNasme", "dcvefr"));
		//fail("Should not get here .. Exception Expected");
	}

	// Test Number: 7
	// Test Objective: Check if the register information could be added to database
	// Input(s): register information
	// Expected Output(s): Exception "Should not reach here ... no exception
	
	@Test
	public void test007() throws LibExceptionHandler, SQLException {
		User test7 = new User();

		assertEquals(true,User.Register("papghpapi", "pass", "Osksar", "Klossowski", "fNasme", "546484"));
		//fail("Should not get here .. Exception Expected");
	}
	
	// Test Number: 8
	// Test Objective: Check if the register information could be added to database
	// Input(s): register information
	// Expected Output(s): Exception "Should not reach here ... no exception
	public void test008() throws LibExceptionHandler, SQLException {
		User test8 = new User();

		assertEquals(false,User.Register("oskar", "pass", "Osksar", "Klossowski", "fNasme", "4956465454545646546546546546546545646865"));
		//fail("Should not get here .. Exception Expected");
	}
	
	
}
