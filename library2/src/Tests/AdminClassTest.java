package Tests;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Controller.AdminClass;
import LibraryException.LibExceptionHandler;
import Model.Overdue;
import UserDAO.DBConn;
import View.CalcFine;
import View.Overdue_BooksGUI;
import junit.framework.TestCase;
import static org.junit.Assert.assertNotEquals;

public class AdminClassTest extends TestCase{

	//Test No: 1
	//TestObj: not null
	//Inputs: aaa aaa aaa aaa
	//Outputs: Object not null
	public void test1() throws LibExceptionHandler {
		DBConn test1= new DBConn();
		try{ 
			assertNotNull(test1.adminLogin("aaa", "aaa", "aaa", "aaa"));
		}catch(Exception e){fail("Should not reach here");}
	}

	//Test No: 2
	//TestObj: null
	//Inputs: aaa aaa aaa bbb
	//Outputs: Object Null
	public void test2() throws LibExceptionHandler {
		DBConn test2= new DBConn();
		try{
			assertNull(test2.adminLogin("aaa", "aaa", "aaa", "bbb"));
		}	catch(Exception e){fail("Should not reach here");}
	}

	//Test No: 3
	//TestObj: null
	//Inputs: aaa aaa bbb aaa
	//Outputs: Object Null
	public void test3() throws LibExceptionHandler {
		DBConn test3= new DBConn();
		try{
			assertNull(test3.adminLogin("aaa", "aaa", "bbb", "aaa"));
		}	catch(Exception e){fail("Should not reach here");}
	}
	//Test No: 4
	//TestObj: null
	//Inputs: aaa bbb aaa aaa
	//Outputs: Object Null
	public void test4() throws LibExceptionHandler {
		DBConn test4= new DBConn();
		try{
			assertNull(test4.adminLogin("aaa", "bbb", "aaa", "aaa"));
		}	catch(Exception e){fail("Should not reach here");}
	}			
	//Test No: 5
	//TestObj: null
	//Inputs: bbb aaa aaa aaa
	//Outputs: Object Null
	public void test5() throws LibExceptionHandler {
		DBConn test5= new DBConn();
		try{
			assertNull(test5.adminLogin("bbb", "aaa", "aaa", "aaa"));
		}	catch(Exception e){fail("Should not reach here");}
	}
	//Test No: 6
	//TestObj: null
	//Inputs: "" "" "" ""
	//Outputs: Object Null
	public void test6() throws LibExceptionHandler {
		DBConn test6= new DBConn();
		try{
			assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){fail("Should not reach here");}
	}	

	//Test No: 7
	//TestObj: String obj matches string object
	//Inputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test7() throws LibExceptionHandler {
		Overdue_BooksGUI test7= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, aaa, 2017-03-25, 2017-03-28, 4, 8.0");

		try{
			assertEquals(overdueList, test7.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){fail("Should not reach here");}
	}

	//Test No: 8
	//TestObj: Username
	//Inputs: bbb, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test8() throws LibExceptionHandler {
		Overdue_BooksGUI test8= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("bbb, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test8.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){fail("Should not reach here");}
	}
	//Test No: 9
	//TestObj: Book/Journal Title
	//Inputs: aaa, aaa, bbb, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test9() throws LibExceptionHandler {
		Overdue_BooksGUI test9= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, bbb, 2017-03-25, 2017-03-28, 3, 6.0");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test9.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){fail("Should not reach here");}
	}
	//Test No: 10
	//TestObj: Expiry Date
	//Inputs: aaa, aaa, bbb, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test10() throws LibExceptionHandler {
		Overdue_BooksGUI test10= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, aaa, 2017-03-25, 2017-04-28, 3, 6.0");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test10.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){}
	}	
	//Test No: 009
	//TestObj: String obj matches string object User
	//Inputs: aaa, aaa, aaa, 2017-04-01, 2017-04-02
	//Outputs: aaa, aaa, aaa, 2017-04-01, 2017-04-02
	public void test009() throws LibExceptionHandler {
		CalcFine test9= new CalcFine();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, aaa, 2017-04-01, 2017-04-02");

		try{
			assertEquals(overdueList, test9.getData2());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){}
	}

	//Test No: 010
	//TestObj: Username User
	//Inputs: bbb, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test010() throws LibExceptionHandler {
		Overdue_BooksGUI test010= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("bbb, aaa, aaa, 2017-04-01, 2017-04-02");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test010.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){}
	}
	//Test No: 011 User
	//TestObj: Expiry Date
	//Inputs: aaa, aaa, aaa, 2018-04-01, 2017-04-02
	//Outputs: bbb, aaa, aaa, 2017-04-01, 2017-04-02
	public void test22() throws LibExceptionHandler {
		Overdue_BooksGUI test9= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, aaa, 2018-04-01, 2017-04-02");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test9.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){}
	}
	//Test No: 10
	//TestObj: Book/Journal Title
	//Inputs: aaa, aaa, bbb, 2017-03-25, 2017-03-28, 3, 6.0
	//Outputs: aaa, aaa, aaa, 2017-03-25, 2017-03-28, 3, 6.0
	public void test20() throws LibExceptionHandler {
		Overdue_BooksGUI test10= new Overdue_BooksGUI();
		//ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		List<String> overdueList = new ArrayList<String>();
		overdueList.add("aaa, aaa, aaa, 2017-03-25, 2017-04-28, 3, 6.0");

		try{
			String message="fail";
			assertNotSame(message, overdueList, test10.getData());
			//assertNull(test6.adminLogin("", "", "", ""));
		}	catch(Exception e){}
	}
	//Test No: 23
	//TestObj: fine
	//Inputs: 1
	//Outputs: Success
	public void test23() throws LibExceptionHandler {
		CalcFine test23= new CalcFine();
		try{
			assertEquals(1,test23.showFine(1));
		}catch(LibExceptionHandler e){
			fail("Should notreach here");
		}
	}
	//Test No: 24
	//TestObj: fine
	//Inputs: 0
	//Outputs: fail
	public void test24() throws LibExceptionHandler {
		CalcFine test24= new CalcFine();
		try{
			test24.showFine(-1);
			fail("Should not get here .. Exception Expected");		
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 25
	//TestObj: fine
	//Inputs: -1
	//Outputs: fail
	public void test25() throws LibExceptionHandler {
		CalcFine test25= new CalcFine();
		try{
			test25.showFine(-1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 26
	//TestObj: fine
	//Inputs: --Integer.MAX_VALUE
	//Outputs: fail
	public void test26() throws LibExceptionHandler {
		CalcFine test26= new CalcFine();
		try{
			assertEquals(2147483646 ,test26.showFine(Integer.MAX_VALUE-1));
		}catch(LibExceptionHandler e){
			fail("Should notreach here");
		}
	}
	//Test No: 27
	//TestObj: fine
	//Inputs: +Integer.MAX_VALUE
	//Outputs: fail
	public void test27() throws LibExceptionHandler {
		CalcFine test27= new CalcFine();
		try{
			test27.showFine(Integer.MAX_VALUE +1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 28
	//TestObj: fine
	//Inputs: Integer.MIN_VALUE +1
	//Outputs: fail
	public void test28() throws LibExceptionHandler {
		CalcFine test28= new CalcFine();
		try{
			test28.showFine(Integer.MIN_VALUE +1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}	
	//Test No: 29
	//TestObj: fine
	//Inputs: Integer.MIN_VALUE -1
	//Outputs: fail
	public void test29() throws LibExceptionHandler {
		CalcFine test29= new CalcFine();
		try{
			test29.showFine(Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}	

	//Test No: 30
	//TestObj: fine
	//Inputs: Integer.MIN_VALUE -1
	//Outputs: fail
	public void test30() throws LibExceptionHandler {
		CalcFine test30= new CalcFine();
		try{
			test30.showFine(Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}

	//Test No: 31
	//TestObj: fine
	//Inputs: 1
	//Outputs: Success
	public void test31() throws LibExceptionHandler {
		Overdue_BooksGUI test31= new Overdue_BooksGUI();
		try{
			assertEquals(1,test31.showTotal(1));
		}catch(LibExceptionHandler e){
			fail("Should notreach here");
		}
	}
	//Test No: 32
	//TestObj: fine
	//Inputs: 0
	//Outputs: fail
	public void test32() throws LibExceptionHandler {
		Overdue_BooksGUI test32= new Overdue_BooksGUI();
		try{
			test32.showTotal(-1);
			fail("Should not get here .. Exception Expected");		
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 33
	//TestObj: fine
	//Inputs: -1
	//Outputs: fail
	public void test33() throws LibExceptionHandler {
		Overdue_BooksGUI test33= new Overdue_BooksGUI();
		try{
			test33.showTotal(-1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 34
	//TestObj: fine
	//Inputs: --Integer.MAX_VALUE
	//Outputs: fail
	public void test34() throws LibExceptionHandler {
		Overdue_BooksGUI test34= new Overdue_BooksGUI();
		try{
			assertEquals(2147483646 ,test34.showTotal(Integer.MAX_VALUE-1));
		}catch(LibExceptionHandler e){
			fail("Should notreach here");
		}
	}
	//Test No: 35
	//TestObj: fine
	//Inputs: +Integer.MAX_VALUE
	//Outputs: fail
	public void test35() throws LibExceptionHandler {
		Overdue_BooksGUI test35= new Overdue_BooksGUI();
		try{
			test35.showTotal(Integer.MAX_VALUE +1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
	//Test No: 36
	//TestObj: fine
	//Inputs: Integer.MIN_VALUE +1
	//Outputs: fail
	public void test36() throws LibExceptionHandler {
		Overdue_BooksGUI test36= new Overdue_BooksGUI();
		try{
			test36.showTotal(Integer.MIN_VALUE +1);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}	
		

	//Test No: 37
	//TestObj: fine
	//Inputs: Integer.MIN_VALUE -1
	//Outputs: fail
	public void test40() throws LibExceptionHandler {
		Overdue_BooksGUI test40= new Overdue_BooksGUI();
		try{
			test40.showTotal(Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		}catch(LibExceptionHandler e){
			assertEquals("Cannot be negitive",e.getMessage());
		}
	}
}
