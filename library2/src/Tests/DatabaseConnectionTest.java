package Tests;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;
import junit.framework.TestCase;

public class DatabaseConnectionTest extends TestCase{
// Database Connection test
	@Test
	//Test No: 1
	//TestObj: Valid Input
	//Inputs: 'aaa'
	//Outputs: Object will not be null
	public void test1() {
		 DBConn test1 = new DBConn();
		 try{
			 assertNotNull(test1.getConnection("aaa"));
			}catch(LibExceptionHandler e){
				fail("Should not reach here");
			}
	}
	@Test
	//Test No: 2
	//TestObj: Invalid Input
	//Inputs: 'bbb'
	//Outputs: Object will be null
	public void test2() {
		 DBConn test2 = new DBConn();
		 try{
			 assertNotNull(null,test2.getConnection("bbb"));
			}catch(LibExceptionHandler e){
				fail("Should not reach here");
			}
	}
	//Database close Test
	
	@Test
	//Test No: 3
	//TestObj: Close Connection
	//Inputs: false
	//Outputs: true
	public void test3() throws LibExceptionHandler {
		 DBConn test3 = new DBConn();
		 try {
			assertEquals(true,test3.closeConn());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Should not reach here");
		}
	}
	
}
