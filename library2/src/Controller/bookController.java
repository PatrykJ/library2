package Controller;

import java.sql.*;

import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;

public class bookController {

	static DBConn myDB = new DBConn();

	public boolean addBook(String title, String author, String genre, String publisher, String editionNumber,
			String isbn, String libNumber) throws LibExceptionHandler {

		if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || publisher.isEmpty() || editionNumber.isEmpty()
				|| isbn.isEmpty() || libNumber.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");

		System.out.println(title + author + genre + publisher + editionNumber + isbn + libNumber);

		boolean res = myDB.addBook(title, author, genre, publisher, editionNumber, isbn, libNumber);
		if (res) {
			return res;
		}
		return false;

	}

	public boolean updateBook(String title, String author, String genre, String publisher, String editionNumber,
			String isbn, String libNumber) throws LibExceptionHandler {

		if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || publisher.isEmpty() || editionNumber.isEmpty()
				|| isbn.isEmpty() || libNumber.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");

		boolean res = myDB.updateBook(title, author, genre, publisher, editionNumber, isbn, libNumber);
		if (res) {
			return true;
		}
		return false;

	}

	public boolean addJornals(String title, String author, String genre, String publisher, String editionNumber,
			String libNumber) throws LibExceptionHandler {

		if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || publisher.isEmpty() || editionNumber.isEmpty()
				|| libNumber.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");
		boolean res = myDB.addJornal(title, author, genre, publisher, editionNumber, libNumber);
		if (res) {
			return res;
		}
		return false;
	}

	public boolean updateJornals(String title, String author, String genre, String publisher, String editionNumber,
			String libNumber) throws LibExceptionHandler {

		if (title.isEmpty() || author.isEmpty() || genre.isEmpty() || publisher.isEmpty() || editionNumber.isEmpty()
				|| libNumber.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");

		int res = myDB.updateJornal(title, author, genre, publisher, editionNumber, libNumber);
		if (res > 0) {
			return true;
		}
		return false;
	}

	public String searchBook(String textField, String search_combo) {
		String res = myDB.searchBook(textField, search_combo);
		return res;

	}

}
