package Controller;
import java.sql.*;

import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;

public class requestController {

	static DBConn myDB = new DBConn();

	public boolean addBookRequest(String isbn, String docName, String days) throws LibExceptionHandler {

		if (isbn.isEmpty() || docName.isEmpty() || days.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");

		System.out.println(isbn + docName + days);

		boolean res = myDB.addBookRequest(isbn, docName, days);
		if (res) {
			return res;
		}
		return false;

	}

	
	public boolean addJournalRequest(String docName, String days) throws LibExceptionHandler {

		if ( docName.isEmpty() || days.isEmpty())
			throw new LibExceptionHandler("one or more text is missing");

		System.out.println(docName + days);

		boolean res = myDB.addJournalRequest(docName, days);
		if (res) {
			return res;
		}
		return false;

	}
	

}
