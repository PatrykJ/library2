package View;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.AdminClass;
import LibraryException.LibExceptionHandler;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;

public class AdminLoginGui extends JFrame {

	private JPanel contentPane;
	private JTextField nameText;
	private JTextField nicknameText;
	private JTextField emailText;
	private JTextField passwordText;
	private JFrame frame;

	public AdminLoginGui() {
		
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.info);
		frame.setVisible(true);
		frame.setBounds(100, 100, 600, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		nameText = new JTextField();
		nameText.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		nameText.setBounds(254, 117, 239, 28);
		frame.getContentPane().add(nameText);
		nameText.setColumns(10);
		
		nicknameText = new JTextField();
		nicknameText.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		nicknameText.setBounds(254, 177, 239, 28);
		frame.getContentPane().add(nicknameText);
		nicknameText.setColumns(10);
		
		emailText = new JTextField();
		emailText.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		emailText.setBounds(254, 238, 239, 28);
		frame.getContentPane().add(emailText);
		emailText.setColumns(10);
		
		JLabel nameLabel = new JLabel("NAME:");
		nameLabel.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		nameLabel.setBounds(74, 115, 158, 28);
		frame.getContentPane().add(nameLabel);
		
		JButton btnLoginAdmin = new JButton("ADMIN LOGIN");
		btnLoginAdmin.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnLoginAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = nameText.getText();
				String nickname = nicknameText.getText();
				String email = emailText.getText();
				String password = passwordText.getText();
				
				try {
					boolean ab = AdminClass.checkLogin(name, nickname, email, password);
					System.out.println("AdminLoginGui"+ab);
					if (ab== true)
					{
						AdminHome a=new AdminHome();
						frame.setVisible(false);
					}
					else{
						JOptionPane.showMessageDialog(null, "Wrong Credentials");}}
								
				 catch (LibExceptionHandler e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnLoginAdmin.setBounds(191, 366, 196, 36);
		frame.getContentPane().add(btnLoginAdmin);
		
		passwordText = new JTextField();
		passwordText.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		passwordText.setBounds(254, 299, 239, 28);
		frame.getContentPane().add(passwordText);
		passwordText.setColumns(10);
		
		JLabel lblADM = new JLabel("A D M I N   L O G I N");
		lblADM.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblADM.setBounds(147, 38, 391, 36);
		frame.getContentPane().add(lblADM);
		
		JLabel lblNickname = new JLabel("NICKNAME:");
		lblNickname.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblNickname.setBounds(74, 179, 158, 28);
		frame.getContentPane().add(lblNickname);
		
		JLabel lblEmail = new JLabel("EMAIL:");
		lblEmail.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblEmail.setBounds(74, 236, 158, 28);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblPassword = new JLabel("PASSWORD:");
		lblPassword.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblPassword.setBounds(74, 297, 158, 28);
		frame.getContentPane().add(lblPassword);
	}
}
