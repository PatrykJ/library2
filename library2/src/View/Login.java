package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import library2.User;
import LibraryException.LibExceptionHandler;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame{

	private static JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Login window = new Login();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 225));
		frame.setBounds(0, -47, 566, 425);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);		
		
		JLabel lblLOG = new JLabel("L O G   I N");
		lblLOG.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblLOG.setBounds(190, 39, 172, 36);
		frame.getContentPane().add(lblLOG);
		
		JLabel lblName = new JLabel("USER NAME:");
		lblName.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblName.setBounds(77, 109, 149, 28);
		frame.getContentPane().add(lblName);
		
		JLabel lblPassword = new JLabel("E-MAIL:");
		lblPassword.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblPassword.setBounds(77, 170, 149, 28);
		frame.getContentPane().add(lblPassword);
		
		JLabel lblAddress = new JLabel("PASSWORD:");
		lblAddress.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblAddress.setBounds(77, 231, 149, 28);
		frame.getContentPane().add(lblAddress);
		
		textField = new JTextField();
		textField.setFont(new Font("Bookman Old Style", Font.PLAIN, 20));
		textField.setBounds(238, 111, 215, 28);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Bookman Old Style", Font.PLAIN, 20));
		textField_1.setBounds(238, 172, 215, 28);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Bookman Old Style", Font.PLAIN, 20));
		textField_2.setBounds(238, 231, 215, 27);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton = new JButton("LOG IN");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField.getText();
				String email = textField_1.getText();
				String pass = textField_2.getText();
				try {
					User.login(name, email, pass);
				} catch (LibExceptionHandler e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton.setBounds(316, 299, 137, 28);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnRegister = new JButton("REGISTER");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Register().setVisible(true);
				frame.dispose();
			}

		});
		btnRegister.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnRegister.setBounds(77, 299, 137, 28);
		frame.getContentPane().add(btnRegister);
		
		frame.setLocationRelativeTo(null);
	}

	public void setVisible(boolean b) {
		Login window = new Login();
		window.frame.setVisible(b);
	}
	
	public static JFrame getMainFrame()
	{
		return frame;
	}
}