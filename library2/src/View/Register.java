package View;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.UIManager;

import library2.User;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Register extends JFrame{
	static JFrame frame;
	private JTextField username;
	private JTextField password;
	private JTextField confirmpass;
	private JTextField email;
	private JTextField studentName;
	private JTextField studentSurname;
	private JTextField studentID;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register window = new Register();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public Register() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 225));
		frame.setBounds(100, 100, 493, 629);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRegister = new JLabel("R E G I S T E R");
		lblRegister.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblRegister.setBounds(122, 26, 274, 52);
		frame.getContentPane().add(lblRegister);
		
		username = new JTextField();
		username.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		username.setBounds(219, 106, 205, 30);
		frame.getContentPane().add(username);
		username.setColumns(10);
		
		password = new JTextField();
		password.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		password.setBounds(219, 164, 205, 30);
		frame.getContentPane().add(password);
		password.setColumns(10);
		
		confirmpass = new JTextField();
		confirmpass.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		confirmpass.setBounds(219, 216, 205, 30);
		frame.getContentPane().add(confirmpass);
		confirmpass.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("USER NAME:");
		lblNewLabel.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblNewLabel.setBounds(45, 106, 173, 30);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("PASSWORD:");
		lblNewLabel_1.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblNewLabel_1.setBounds(45, 166, 173, 23);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("CONFIRM PASS:");
		lblNewLabel_2.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblNewLabel_2.setBounds(45, 218, 173, 29);
		frame.getContentPane().add(lblNewLabel_2);
		
		email = new JTextField();
		email.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		email.setBounds(219, 269, 205, 30);
		frame.getContentPane().add(email);
		email.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("E-MAIL:");
		lblNewLabel_3.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblNewLabel_3.setBounds(45, 271, 173, 30);
		frame.getContentPane().add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("SUBMIT");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Username = username.getText();
				String Password = password.getText();
				String Confirmpass = confirmpass.getText();
				String Email = email.getText();
				String name = studentName.getText();
				String surname = studentSurname.getText();
				String ID = studentID.getText();
				
				try {
					User.existingUser(Username, Password, Confirmpass, Email, name, surname, ID);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton.setBounds(151, 502, 156, 40);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblName = new JLabel("FIRSTNAME:");
		lblName.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblName.setBounds(45, 326, 173, 30);
		frame.getContentPane().add(lblName);
		
		JLabel lblSurname = new JLabel("LASTNAME:");
		lblSurname.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblSurname.setBounds(45, 380, 173, 30);
		frame.getContentPane().add(lblSurname);
		
		JLabel lblStudentId = new JLabel("STUDENT ID:");
		lblStudentId.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblStudentId.setBounds(45, 433, 173, 30);
		frame.getContentPane().add(lblStudentId);
		
		studentName = new JTextField();
		studentName.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		studentName.setColumns(10);
		studentName.setBounds(219, 324, 205, 30);
		frame.getContentPane().add(studentName);
		
		studentSurname = new JTextField();
		studentSurname.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		studentSurname.setColumns(10);
		studentSurname.setBounds(219, 378, 205, 30);
		frame.getContentPane().add(studentSurname);
		
		studentID = new JTextField();
		studentID.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		studentID.setColumns(10);
		studentID.setBounds(219, 433, 205, 30);
		frame.getContentPane().add(studentID);
	}
	
	public void setVisible(boolean bb)
	{
		frame.setVisible(bb);
	}
	
	public final JFrame getRegisterFrame(){
        return frame;
    }
	
    public final static JFrame getMainFrame(){
        return frame;
    }
}
