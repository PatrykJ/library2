package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JLabel;

public class ChooseStudentOrAdminPage {

	private JFrame frame;

	public ChooseStudentOrAdminPage() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.info);
		frame.setVisible(true);
		frame.setBounds(100, 100, 696, 262);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnStudent = new JButton("STUDNET");
		btnStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentLoginGUI studentlogin = new StudentLoginGUI();
				frame.setVisible(false);
			}
		});
		btnStudent.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnStudent.setBounds(125, 122, 147, 37);
		frame.getContentPane().add(btnStudent);

		JButton btnAdmin = new JButton("ADMIN");
		btnAdmin.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminLoginGui a = new AdminLoginGui();
				frame.setVisible(false);
			}
		});
		btnAdmin.setBounds(387, 122, 147, 37);
		frame.getContentPane().add(btnAdmin);
		
		JLabel lblIdentificationChoice = new JLabel("I D E N T I F I C A T I O N   C H O I C E");
		lblIdentificationChoice.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblIdentificationChoice.setBounds(54, 36, 624, 37);
		frame.getContentPane().add(lblIdentificationChoice);
	}
}
