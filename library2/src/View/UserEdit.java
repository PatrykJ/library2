package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import library2.User;
import library2.userBean;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;

public class UserEdit extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtSurname;
	private JTextField txtPhone;
	private JTextField txtPass;
	private JTextField txtPass2;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					UserEdit frame = new UserEdit();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public UserEdit() {	
		//txtName.setText("Oskar is handsome");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 556, 703);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.info);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("E D I T U S E R  D E T A I L S");
		lblNewLabel.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblNewLabel.setBounds(68, 34, 462, 34);
		contentPane.add(lblNewLabel);
		
		JLabel lblName = new JLabel("NAME:");
		lblName.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblName.setBounds(43, 104, 213, 34);
		contentPane.add(lblName);
		
		JLabel lblChangePassowrd = new JLabel("C H A N G E   P A S S W O R D");
		lblChangePassowrd.setFont(new Font("Bookman Old Style", Font.ITALIC, 28));
		lblChangePassowrd.setBounds(68, 325, 462, 34);
		contentPane.add(lblChangePassowrd);
		
		txtName = new JTextField();
		txtName.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		txtName.setBounds(246, 106, 249, 34);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		txtSurname = new JTextField();
		txtSurname.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		txtSurname.setBounds(246, 166, 249, 34);
		contentPane.add(txtSurname);
		txtSurname.setColumns(10);
		
		txtPhone = new JTextField();
		txtPhone.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		txtPhone.setBounds(246, 228, 249, 34);
		contentPane.add(txtPhone);
		txtPhone.setColumns(10);
		
		txtPass = new JTextField();
		txtPass.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		txtPass.setBounds(246, 403, 249, 34);
		contentPane.add(txtPass);
		txtPass.setColumns(10);
		
		txtPass2 = new JTextField();
		txtPass2.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		txtPass2.setBounds(246, 472, 249, 34);
		contentPane.add(txtPass2);
		txtPass2.setColumns(10);
		
		txtName.setText(userBean.getName());
		txtSurname.setText(userBean.getSurname());
		txtPhone.setText(userBean.getMobile());
		
		JButton btnApplyChange = new JButton("APPLY CHANGE");
		btnApplyChange.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnApplyChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				
				int id = Integer.parseInt(userBean.getId());
				String name=txtName.getText();
				String surname=txtSurname.getText();
				String phone=txtPhone.getText();
				
				String pwd=txtPass.getText();
				String pwd2=txtPass2.getText();
				
				if(!pwd.equals(null))
				{
					User.passwordCheck(pwd, pwd2);
				}
				else 
				{
					User.changeDetails(name, surname, phone, id);
				}
			}
		});
		btnApplyChange.setBounds(32, 564, 213, 43);
		contentPane.add(btnApplyChange);
		
		JButton btnCancel = new JButton("CANCEL");
		btnCancel.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnCancel.setBounds(281, 564, 214, 43);
		contentPane.add(btnCancel);
		
		JLabel lblSurname = new JLabel("SURNAME:");
		lblSurname.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblSurname.setBounds(43, 166, 213, 34);
		contentPane.add(lblSurname);
		
		JLabel lblPhoneNumber = new JLabel("PHONE NUMBER:");
		lblPhoneNumber.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblPhoneNumber.setBounds(43, 233, 213, 34);
		contentPane.add(lblPhoneNumber);
		
		JLabel lblPassword = new JLabel("PASSWORD:");
		lblPassword.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblPassword.setBounds(32, 403, 213, 34);
		contentPane.add(lblPassword);
		
		JLabel lblConfirmPass = new JLabel("CONFIRM PASS:");
		lblConfirmPass.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblConfirmPass.setBounds(32, 472, 213, 34);
		contentPane.add(lblConfirmPass);
		
		//frame.setLocationRelativeTo(null);
	}
}
