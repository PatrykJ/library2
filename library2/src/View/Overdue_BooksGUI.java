package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;

import Controller.AdminClass;
import LibraryException.LibExceptionHandler;
import Model.Overdue;
import UserDAO.DBConn;
import library2.User;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;

public class Overdue_BooksGUI {

	private static JFrame frame;
	private JTextField searchByID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Overdue_BooksGUI window = new Overdue_BooksGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Overdue_BooksGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		JButton btnSearchAll = new JButton("Search All");
		btnSearchAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getData();
				
			}
		});
		btnSearchAll.setBounds(20, 163, 89, 23);
		frame.getContentPane().add(btnSearchAll);
		
		JButton btnSeeHowMuch = new JButton("See how much is due");
		btnSeeHowMuch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Overdue> allOverDueDocs = null;
				try {
					allOverDueDocs = AdminClass.getAllOverDueDocuments();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (LibExceptionHandler e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//textAreaFines.setText(" ");
				int total=0;
				for(Overdue overdueStudent : allOverDueDocs)
				{
					if(overdueStudent.getFine()>0){
					total+=overdueStudent.getFine();
					System.out.println(total);}
					
				}
				try {
					showTotal(total);
				} catch (LibExceptionHandler e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSeeHowMuch.setBounds(158, 163, 215, 23);
		frame.getContentPane().add(btnSeeHowMuch);
		
		
	}
	public List<String> getData(){
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 11, 414, 132);
		frame.getContentPane().add(textArea);
		String p="";
		List<Overdue> abc = new ArrayList<Overdue>();
		List<String> bca = new ArrayList<String>();
		try {
			List<Overdue> allOverDueDocs = AdminClass.getAllOverDueDocuments();
			
			abc = AdminClass.getAllOverDueDocuments();
			textArea.setText("");
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			
			for(Overdue overdue : abc)
			{
				bca.add(overdue.getStudent_id()+", "+overdue.getISBN()+", "+overdue.getDocName()+", "+overdue.getIssue_Date()+", "+overdue.getEnd_Date()+", "+overdue.getOverDueBy()+", "+overdue.getFine());
				
					p = "ID: "+ overdue.getStudent_id()+"\n"
						+"ISBN: "+ overdue.getISBN()+"\n"
						+"Title: "+ overdue.getDocName()+"\n"
						+"IssueDate: "+ overdue.getIssue_Date()+"\n"
						+"EndDate: "+ overdue.getEnd_Date()+"\n"
						+"OverDue By "+ overdue.getOverDueBy()+" Days \n"
						+"Fine �"+overdue.getFine()+"\n";
					
				textArea.append(p);
				//List<Overdue> abc = new ArrayList<Overdue>();
				
				
				
				System.out.println("ID: "+ overdue.getStudent_id());
				System.out.println("ISBN: "+ overdue.getISBN());
				System.out.println("Title: "+ overdue.getDocName());
				System.out.println("IssueDate: "+ overdue.getIssue_Date());
				System.out.println("EndDate: "+ overdue.getEnd_Date());
			}
		} catch (LibExceptionHandler e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return bca;
	}
	public long showTotal(int total) throws LibExceptionHandler{
		if(total <=0 &&total < Integer.MAX_VALUE)throw new LibExceptionHandler("Cannot be negitive");
			
		else{ 
			JOptionPane.showMessageDialog(null, "Total Owed �"+total);
		return total;}

	}
}
