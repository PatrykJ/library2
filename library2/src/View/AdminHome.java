package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class AdminHome extends JFrame {

	private JFrame frame;

	public AdminHome() {

		frame = new JFrame();
		frame.setVisible(true);
		frame.getContentPane().setBackground(new Color(255, 255, 225));
		frame.setBounds(100, 100, 624, 487);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("A D M I N   H O M E");
		lblNewLabel.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblNewLabel.setBounds(144, 37, 300, 35);
		frame.getContentPane().add(lblNewLabel);

		JButton btnAddBookjournal = new JButton("ADD BOOK/JOURNAL");
		btnAddBookjournal.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnAddBookjournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BookGUI bGui = new BookGUI();
			}
		});
		btnAddBookjournal.setBounds(144, 116, 300, 35);
		frame.getContentPane().add(btnAddBookjournal);

		JButton btnNewButton = new JButton("UPDATE BOOK/JOURNAL");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdatedBookGUI myUpdate = new UpdatedBookGUI();
			}
		});
		btnNewButton.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton.setBounds(144, 180, 300, 35);
		frame.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("SEARCH BOOK/JOURNAL");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SearchGUI mySearch = new SearchGUI();
			}
		});
		btnNewButton_1.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_1.setBounds(144, 243, 300, 35);
		frame.getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("VIEW REQUEST");
		btnNewButton_2.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_2.setBounds(144, 305, 300, 35);
		frame.getContentPane().add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("NOTIFICATIONS");
		btnNewButton_3.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_3.setBounds(144, 364, 300, 35);
		frame.getContentPane().add(btnNewButton_3);

		JButton btnNewButton_4 = new JButton("LOG OUT");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnNewButton_4.setFont(new Font("Bookman Old Style", Font.ITALIC, 16));
		btnNewButton_4.setBounds(480, 13, 114, 35);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnViewOverueDocs = new JButton("View Overdue Docs");
		btnViewOverueDocs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Overdue_BooksGUI overdue = new Overdue_BooksGUI();
				overdue.main(null);
			}
		});
		btnViewOverueDocs.setBounds(230, 83, 126, 23);
		frame.getContentPane().add(btnViewOverueDocs);
	}
}
//