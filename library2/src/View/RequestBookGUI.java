package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import LibraryException.LibExceptionHandler;
import Model.Book;
import Model.LoanBook;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RequestBookGUI {

	private JFrame frame;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblIsbn;
	private JLabel lblBookName;
	private JLabel lblNumberOfDays;
	private JTextField textField_3;
	private JButton btnPlaceRequest;
	private LoanBook LoanBook;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RequestBookGUI window = new RequestBookGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RequestBookGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRequestBook = new JLabel("Request Book");
		lblRequestBook.setBounds(167, 24, 101, 21);
		frame.getContentPane().add(lblRequestBook);
		
		textField_1 = new JTextField();
		textField_1.setBounds(229, 92, 86, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(229, 123, 86, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		lblIsbn = new JLabel("ISBN");
		lblIsbn.setBounds(76, 95, 46, 14);
		frame.getContentPane().add(lblIsbn);
		
		lblBookName = new JLabel("Book Name");
		lblBookName.setBounds(76, 126, 72, 14);
		frame.getContentPane().add(lblBookName);
		
		lblNumberOfDays = new JLabel("Number of days");
		lblNumberOfDays.setBounds(76, 157, 86, 14);
		frame.getContentPane().add(lblNumberOfDays);
		
		textField_3 = new JTextField();
		textField_3.setBounds(229, 154, 86, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		btnPlaceRequest = new JButton("Place Request");
		btnPlaceRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//String username = textField.getText();
				String isbn = textField_1.getText();
				String docName = textField_2.getText();
				String days = textField_3.getText();
				
				
				try {
					LoanBook lb = new LoanBook(isbn, docName, days);
					lb.addBookRequest();
				} catch (LibExceptionHandler e) {
					// TODO Auto-generated catch blockz
					e.printStackTrace();
				}
				
			}
		});
		btnPlaceRequest.setBounds(148, 202, 109, 32);
		frame.getContentPane().add(btnPlaceRequest);
	}
}
