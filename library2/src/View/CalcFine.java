package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Controller.AdminClass;
import LibraryException.LibExceptionHandler;
import Model.Overdue;
import library2.User;

import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class CalcFine {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalcFine window = new CalcFine();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalcFine() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextArea textAreaFines = new JTextArea();
		textAreaFines.setBounds(10, 11, 414, 137);
		frame.getContentPane().add(textAreaFines);
		
		JButton btnSeeAllLoans = new JButton("See all Loans");
		btnSeeAllLoans.addActionListener(new ActionListener() {
			String p="";
			public void actionPerformed(ActionEvent e) {
				getData2();
			}
		});
		btnSeeAllLoans.setBounds(10, 191, 135, 23);
		frame.getContentPane().add(btnSeeAllLoans);
		
		JButton btnSeeFines = new JButton("See Fines");
		btnSeeFines.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Overdue> allstudentOverDueDocs = null;
				try {
					allstudentOverDueDocs = User.getStudentOverDueDocuments();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//textAreaFines.setText(" ");
				int total=0;
				for(Overdue overdueStudent : allstudentOverDueDocs)
				{
					if(overdueStudent.getFine()>0){
					total+=overdueStudent.getFine();
					System.out.println(total);}
					
				}
				try {
					showFine(total);
				} catch (LibExceptionHandler e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSeeFines.setBounds(184, 191, 182, 23);
		frame.getContentPane().add(btnSeeFines);
		
		
	}
	public List<String> getData2(){
		JTextArea textAreaFines = new JTextArea();
		textAreaFines.setBounds(10, 11, 414, 137);
		frame.getContentPane().add(textAreaFines);
		String  p ="";
		List<Overdue> abc = new ArrayList<Overdue>();
		List<String> bca = new ArrayList<String>();

		try {
			abc = User.getStudentOverDueDocuments();
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		textAreaFines.setText(" ");
		
		for(Overdue overdueStudent : abc)
		{
			bca.add(overdueStudent.getStudent_id()+", "+overdueStudent.getISBN()+", "+overdueStudent.getDocName()+", "+overdueStudent.getIssue_Date());
			
				p = "ID: "+ overdueStudent.getStudent_id()+"\n"
					+"ISBN: "+ overdueStudent.getISBN()+"\n"
					+"Title: "+ overdueStudent.getDocName()+"\n"
					+"IssueDate: "+ overdueStudent.getIssue_Date()+"\n"
					+"EndDate: "+ overdueStudent.getEnd_Date()+"\n"
					+"OverDue By "+ overdueStudent.getOverDueBy()+"\n"
					;
				
			textAreaFines.append(p);
			
			System.out.println("ID: "+ overdueStudent.getStudent_id());
			System.out.println("ISBN: "+ overdueStudent.getISBN());
			System.out.println("Title: "+ overdueStudent.getDocName());
			System.out.println("IssueDate: "+ overdueStudent.getIssue_Date());
			System.out.println("EndDate: "+ overdueStudent.getEnd_Date());
		}
		return bca;
	}
	public long showFine(int total) throws LibExceptionHandler{
		if(total <=0 &&total < Integer.MAX_VALUE)throw new LibExceptionHandler("Cannot be negitive");
			
		else{ 
			JOptionPane.showMessageDialog(null, "You owe �"+total);
		return total;}

	}
}
