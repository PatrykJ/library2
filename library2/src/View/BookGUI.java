package View;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import LibraryException.LibExceptionHandler;
import Model.Book;
import java.awt.SystemColor;
import java.awt.Font;

public class BookGUI {
	private JFrame frame;

	private JTextField titleTF;
	private JTextField authorTF;
	private JTextField generTF;
	private JTextField publisherTF;
	private JTextField editionNoTF;
	private JTextField isbnTF;
	private JTextField libraryNoTF;

	private JButton btnAddBook;

	private Book myBook;
	private JLabel lblAuthor;
	private JLabel lblGenre;
	private JLabel lblPublisher;
	private JLabel lblEditionNumber;
	private JLabel lblIsbn;
	private JLabel lblLibraryNumber;
	private JLabel lblBookInformation;

	public BookGUI() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.info);

		Container cp = frame.getContentPane();
		JLabel titleLabel = new JLabel("TITLE:");
		titleLabel.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		titleLabel.setBounds(39, 107, 221, 37);

		titleTF = new JTextField(10);
		titleTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		titleTF.setBounds(249, 109, 239, 37);
		authorTF = new JTextField(10);
		authorTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		authorTF.setBounds(249, 168, 239, 37);
		generTF = new JTextField(10);
		generTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		generTF.setBounds(249, 227, 239, 37);
		publisherTF = new JTextField(10);
		publisherTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		publisherTF.setBounds(249, 288, 239, 37);
		editionNoTF = new JTextField(10);
		editionNoTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		editionNoTF.setBounds(249, 349, 239, 37);
		isbnTF = new JTextField(10);
		isbnTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		isbnTF.setBounds(249, 410, 239, 37);
		libraryNoTF = new JTextField(10);
		libraryNoTF.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		libraryNoTF.setBounds(249, 471, 239, 37);

		btnAddBook = new JButton("ADD BOOK");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String title = titleTF.getText();
				String author = authorTF.getText();
				String gener = generTF.getText();
				String publisher = publisherTF.getText();
				String editionNo = editionNoTF.getText();
				String isbn = isbnTF.getText();
				String libraryNo = libraryNoTF.getText();
				try {
					myBook = new Book(title, author, gener, publisher, editionNo, isbn, libraryNo);
					boolean res = myBook.addBook();
					if (res) {
						JOptionPane.showMessageDialog(frame, "Book has been added");
						frame.setVisible(false);
					} else {
						JOptionPane.showMessageDialog(frame, "Book not added", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} catch (LibExceptionHandler e1) {
					System.out.println(e1);

				}
			}
		});
		btnAddBook.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnAddBook.setBounds(10, 562, 221, 37);
		frame.getContentPane().setLayout(null);

		cp.add(titleLabel);
		cp.add(titleTF);
		cp.add(authorTF);
		cp.add(generTF);
		cp.add(publisherTF);
		cp.add(editionNoTF);
		cp.add(isbnTF);
		cp.add(libraryNoTF);

		cp.add(btnAddBook);

		lblAuthor = new JLabel("AUTHOR:");
		lblAuthor.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblAuthor.setBounds(39, 166, 221, 37);
		frame.getContentPane().add(lblAuthor);

		lblGenre = new JLabel("GENRE:");
		lblGenre.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblGenre.setBounds(39, 229, 221, 37);
		frame.getContentPane().add(lblGenre);

		lblPublisher = new JLabel("PUBLISHER:");
		lblPublisher.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblPublisher.setBounds(39, 288, 221, 37);
		frame.getContentPane().add(lblPublisher);

		lblEditionNumber = new JLabel("EDITION NUMBER:");
		lblEditionNumber.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblEditionNumber.setBounds(39, 349, 221, 37);
		frame.getContentPane().add(lblEditionNumber);

		lblIsbn = new JLabel("ISBN:");
		lblIsbn.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblIsbn.setBounds(39, 410, 221, 37);
		frame.getContentPane().add(lblIsbn);

		lblLibraryNumber = new JLabel("LIBRARY NUMBER:");
		lblLibraryNumber.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		lblLibraryNumber.setBounds(39, 471, 221, 37);
		frame.getContentPane().add(lblLibraryNumber);

		lblBookInformation = new JLabel("B O O K   I N F O R M A T I O N");
		lblBookInformation.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblBookInformation.setBounds(39, 35, 512, 37);
		frame.getContentPane().add(lblBookInformation);
		
		JButton btnAddJournals = new JButton("ADD JOURNALS");
		btnAddJournals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String title = titleTF.getText();
				String author = authorTF.getText();
				String gener = generTF.getText();
				String publisher = publisherTF.getText();
				String editionNo = editionNoTF.getText();
				String libraryNo = libraryNoTF.getText();
				
				try {
					myBook = new Book(title, author, gener, publisher, editionNo, libraryNo);
					boolean res = myBook.addJournal();;
					if (res) {
						JOptionPane.showMessageDialog(frame, "Book has been added");
						frame.setVisible(false);
					} else {
						JOptionPane.showMessageDialog(frame, "Book not added", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} catch (LibExceptionHandler e1) {
					System.out.println(e1);

				}
			}
		});
		btnAddJournals.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnAddJournals.setBounds(262, 562, 226, 37);
		frame.getContentPane().add(btnAddJournals);

		frame.setSize(539, 692);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
