package View;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Controller.bookController;

import javax.swing.JButton;

import java.sql.*;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class SearchGUI implements ActionListener {

	private JFrame frame;
	private JTextField t1;
	private JTextField t2;
	private JTextField t3;
	private JTextField t4;
	private JTextField t5;
	private JTextField t6;
	private JTextField t7;
	private JTextField t8;
	private JTextField t9;

	String cmd = null;

	private Connection con = null;
	private Statement stmt = null;
	private Statement stmt2 = null;
	private Statement stmt3 = null;
	private ResultSet rs = null;

	private Container content;
	private JTextField textField;
	private JButton btnSearch;
	private JComboBox comboBox;
	private JLabel lblLibraryNumber;
	private JTextField textField_1;
	private bookController myBookContoller;

	public SearchGUI() {

		frame = new JFrame();
		frame.setVisible(true);
		myBookContoller = new bookController();
		frame.setBounds(100, 100, 450, 410);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 114, 82, 14);
		frame.getContentPane().add(lblTitle);

		JLabel lblId = new JLabel("Id");
		lblId.setBounds(10, 78, 56, 14);
		frame.getContentPane().add(lblId);

		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 148, 82, 14);
		frame.getContentPane().add(lblAuthor);

		JLabel lblGenre = new JLabel("Genre");
		lblGenre.setBounds(10, 173, 82, 14);
		frame.getContentPane().add(lblGenre);

		JLabel lblPublisher = new JLabel("Publisher");
		lblPublisher.setBounds(10, 204, 82, 14);
		frame.getContentPane().add(lblPublisher);

		JLabel lblEditionNumber = new JLabel("Edition Number");
		lblEditionNumber.setBounds(10, 235, 120, 14);
		frame.getContentPane().add(lblEditionNumber);

		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setBounds(10, 260, 67, 14);
		frame.getContentPane().add(lblIsbn);

		JLabel lblNumberOfStock = new JLabel("Number of Stock");
		lblNumberOfStock.setBounds(10, 331, 120, 14);
		frame.getContentPane().add(lblNumberOfStock);

		t1 = new JTextField();
		t1.setBounds(170, 75, 203, 20);
		t1.setEditable(false);
		frame.getContentPane().add(t1);
		t1.setColumns(10);

		t2 = new JTextField();
		t2.setBounds(170, 111, 203, 20);
		t2.setEditable(false);
		frame.getContentPane().add(t2);
		t2.setColumns(10);

		t3 = new JTextField();
		t3.setBounds(170, 142, 203, 20);
		t3.setEditable(false);
		frame.getContentPane().add(t3);
		t3.setColumns(10);

		t4 = new JTextField();
		t4.setBounds(170, 170, 203, 20);
		t4.setEditable(false);
		frame.getContentPane().add(t4);
		t4.setColumns(10);

		t5 = new JTextField();
		t5.setBounds(170, 201, 203, 20);
		t5.setEditable(false);
		frame.getContentPane().add(t5);
		t5.setColumns(10);

		t6 = new JTextField();
		t6.setBounds(170, 232, 203, 20);
		t6.setEditable(false);
		frame.getContentPane().add(t6);
		t6.setColumns(10);

		t7 = new JTextField();
		t7.setBounds(169, 263, 204, 20);
		t7.setEditable(false);
		frame.getContentPane().add(t7);
		t7.setColumns(10);

		t8 = new JTextField();
		t8.setBounds(169, 294, 204, 23);
		t8.setEditable(false);
		frame.getContentPane().add(t8);
		t8.setColumns(10);

		t9 = new JTextField();
		t9.setBounds(170, 328, 203, 20);
		t9.setEditable(false);
		frame.getContentPane().add(t9);
		t9.setColumns(10);

		textField = new JTextField();
		textField.setBounds(170, 11, 120, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		btnSearch = new JButton("Search");
		btnSearch.setBounds(10, 11, 109, 20);
		frame.getContentPane().add(btnSearch);

		comboBox = new JComboBox();
		comboBox.setBounds(314, 11, 87, 20);
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Name", "Author", "ISBN", "Release Date", "Genre" }));
		frame.getContentPane().add(comboBox);

		lblLibraryNumber = new JLabel("Library Number");
		lblLibraryNumber.setBounds(10, 298, 126, 14);
		frame.getContentPane().add(lblLibraryNumber);

		JLabel lblSearchPosition = new JLabel("Search Position");
		lblSearchPosition.setBounds(10, 42, 120, 14);
		frame.getContentPane().add(lblSearchPosition);

		textField_1 = new JTextField();
		textField_1.setBounds(170, 44, 203, 20);
		textField_1.setEditable(false);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		btnSearch.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {

		Object target = e.getSource();

		String search_combo = (String) comboBox.getSelectedItem();

		if (target == btnSearch) {

			String res = myBookContoller.searchBook(textField.getText(), search_combo);
			String[] d = res.split(",");
			textField_1.setText(d[0]);
			t1.setText(d[1]);
			t2.setText(d[2]);
			t3.setText(d[3]);
			t4.setText(d[4]);
			t5.setText(d[5]);
			t6.setText(d[6]);
			t7.setText(d[7]);
			t8.setText(d[8]);
			t9.setText(d[9]);

		}



	}
}