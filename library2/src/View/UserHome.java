package View;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;

import library2.User;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserHome extends JFrame{

	private JFrame frame;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					UserHome window = new UserHome();
//					window.frame.setVisible(false);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	/**
	 * Create the application.
	 */
	public UserHome() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 225));
		frame.setBounds(100, 100, 601, 520);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUSE = new JLabel("U S E R   H O M E");
		lblUSE.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblUSE.setBounds(143, 44, 266, 30);
		frame.getContentPane().add(lblUSE);
		
		JButton btnNewButton = new JButton("SEARCH BOOK/JOURNAL");
		btnNewButton.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton.setBounds(113, 116, 313, 30);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("PLACE REQUEST");
		btnNewButton_1.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_1.setBounds(113, 173, 313, 30);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("CALCULATE FINE");
		btnNewButton_2.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				CalcFine calc = new CalcFine();
				calc.main(null);
				
				
			}
		});
		btnNewButton_2.setBounds(113, 228, 313, 30);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("INTER-LIBRARY LOAN");
		btnNewButton_3.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_3.setBounds(113, 286, 313, 30);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("LOG OUT");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnNewButton_4.setFont(new Font("Bookman Old Style", Font.ITALIC, 16));
		btnNewButton_4.setBounds(448, 13, 123, 36);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("NOTIFICATIONS");
		btnNewButton_5.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnNewButton_5.setBounds(111, 349, 315, 30);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnChangeDetails = new JButton("CHANGE DETAILS");
		btnChangeDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new UserEdit().setVisible(true);
				frame.dispose();
			}
		});
		btnChangeDetails.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnChangeDetails.setBounds(113, 407, 315, 30);
		frame.getContentPane().add(btnChangeDetails);
		
		frame.setLocationRelativeTo(null);
		
		
	}
	
	
	public void setVisible()
	{
		UserHome window = new UserHome();
		window.frame.setVisible(true);
	}
}