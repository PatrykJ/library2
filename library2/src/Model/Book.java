package Model;

import javax.swing.JOptionPane;

import Controller.bookController;
import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;

public class Book {
	String title;
	String author;
	String genre;
	String publisher;
	String editionNumber;
	String isbn;
	String libNumber;
	static DBConn myDB = new DBConn();
	private bookController myBookControl = new bookController();

	public Book(String t, String a, String g, String p, String e, String i, String l) {
		this.title = t;
		this.author = a;
		this.genre = g;
		this.publisher = p;
		this.editionNumber = e;
		this.isbn = i;
		this.libNumber = l;
	}

	public Book(String t, String a, String g, String p, String e, String l) {
		this.title = t;
		this.author = a;
		this.genre = g;
		this.publisher = p;
		this.editionNumber = e;
		this.libNumber = l;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getEditionNumber() {
		return editionNumber;
	}

	public void setEditionNumber(String editionNumber) {
		this.editionNumber = editionNumber;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getLibNumber() {
		return libNumber;
	}

	public void setLibNumber(String libNumber) {
		this.libNumber = libNumber;
	}

	public boolean addBook() throws LibExceptionHandler {

		boolean res = myBookControl.addBook(title, author, genre, publisher, editionNumber, isbn, libNumber);
		if (res) {
			return true;
		}
		return false;

	}

	public boolean updateBook() throws LibExceptionHandler {
		boolean res = myBookControl.updateBook(title, author, genre, publisher, editionNumber, isbn, libNumber);
		if (res) {
			return true;
		}
		return false;
	}

	public boolean addJournal() throws LibExceptionHandler {
		boolean res = myBookControl.addJornals(title, author, genre, publisher, editionNumber, libNumber);
		return res;
	}

	public boolean updateJournal() throws LibExceptionHandler {

		boolean res = myBookControl.updateJornals(title, author, genre, publisher, editionNumber, libNumber);

		return res;
	}

}
