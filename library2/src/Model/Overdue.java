package Model;

public class Overdue {
	private String userName;
	private String ISBN;
	private String DocName;
	private String Issue_Date;
	private String End_Date;
	private int OverDueBy;
	private double fine;
	public Overdue(String sid, String isbn,String DocName,String id,String ed, int OverDueBy, double fine)
	{
		this.userName=sid; 
		this.ISBN=isbn;
		this.DocName=DocName;
		this.Issue_Date=id;
		this.End_Date=ed;
		this.OverDueBy=OverDueBy;
		this.fine=fine;
	}
	public double getFine() {
		return fine;
	}
	public void setFine(int fine) {
		this.fine = fine;
	}
	public int getOverDueBy() {
		return OverDueBy;
	}
	public void setOverDueBy(int overDueBy) {
		OverDueBy = overDueBy;
	}
	public String getDocName() {
		return DocName;
	}
	public void setDocName(String docName) {
		DocName = docName;
	}
	public String getStudent_id() {
		return userName;
	}
	public void setStudent_id(String userName) {
		this.userName = userName;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public String getIssue_Date() {
		return Issue_Date;
	}
	public void setIssue_Date(String issue_Date) {
		Issue_Date = issue_Date;
	}
	public String getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}
}
