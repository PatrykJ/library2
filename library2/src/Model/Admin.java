package Model;

public class Admin {
	private int id;
	private String UserName;
	private String NickName;
	private String pass;
	private String firstName;
	private String lastName;
	private String email;
	private int MobileNum;
	
	public Admin(int i, String u,String n,String p, String fn,String ln, String e, int m)
	{
		this.id=i;
		this.UserName = n;
		this.NickName = n;
		this.pass = p;
		this.firstName=fn;
		this.lastName=ln;
		this.email=e;
		this.MobileNum=m;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getNickName() {
		return NickName;
	}

	public void setNickName(String nickName) {
		NickName = nickName;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getMobileNum() {
		return MobileNum;
	}

	public void setMobileNum(int mobileNum) {
		MobileNum = mobileNum;
	}
	
	
}
