package Model;


import Controller.requestController;
import LibraryException.LibExceptionHandler;
import UserDAO.DBConn;

public class LoanBook {

	String UserName;
	String ISBN;
	String DocName;
	String Issue_Date;
	String End_Date;
	

	public LoanBook(String u, String i, String d, String is, String ed) {
		this.UserName=u;
		this.ISBN=i;
		this.DocName=d;
		this.Issue_Date=is;
		this.End_Date=ed;	
	}
	
	public LoanBook(String i, String d, String is) {
		this.ISBN=i;
		this.DocName=d;
		this.Issue_Date=is;	
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public String getDocName() {
		return DocName;
	}

	public void setDocName(String docName) {
		DocName = docName;
	}

	public String getIssue_Date() {
		return Issue_Date;
	}

	public void setIssue_Date(String issue_Date) {
		Issue_Date = issue_Date;
	}

	public String getEnd_Date() {
		return End_Date;
	}

	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}
	
	

	public boolean addBookRequest() throws LibExceptionHandler {
		
		DBConn conn = new DBConn();
		boolean res = conn.addBookRequest(ISBN, DocName, Issue_Date);
//		if (res) {
//			return true;
//		}
		return false;

	}
//
//	
//
//	public boolean addJournalRequest() throws LibExceptionHandler {
//		boolean res = myRequestControl.addJournalRequest(docName, days);
//		return res;
//	}

	

	
}
