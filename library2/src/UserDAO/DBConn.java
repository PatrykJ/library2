package UserDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import LibraryException.LibExceptionHandler;
import Model.Admin;
import Model.Overdue;
import View.StudentLoginGUI;
import library2.User;
import library2.userBean;

public class DBConn {

	public static final String DBConn = null;
	private static Connection conn;
	private static String urlstring;
	private static String driverName = "com.mysql.jdbc.Driver";
	private static String userName;
	private static Statement stmt = null;
	private static Statement stmt2 = null;
	private static Statement stmt3 = null;
	private static ResultSet rs = null;

	public static Connection getConnection(String db) throws LibExceptionHandler {
		try {
			String url = "jdbc:mysql://localhost:3306/" + db;
			System.out.println(url);
			Class.forName(driverName);
			try {
				conn = DriverManager.getConnection(url, "karl", "20071985Kh");
				stmt = conn.createStatement();
				stmt2 = conn.createStatement();
				stmt3 = conn.createStatement();
			} catch (SQLException ex) {
				// log an exception. fro example:
				System.out.println("Failed to create the database connection.");
			}
		} catch (ClassNotFoundException ex) {
			// log an exception. for example:
			System.out.println("Driver not found.");
		}
		return conn;

	}

	public static Admin adminLogin(String name, String nickname, String email, String password)
			throws LibExceptionHandler {
		Admin admin = null;
		// Connection conn = getConnection(DBSelection.databaseName);
		Connection connection2 = getConnection("AgileDB");

		try {
			PreparedStatement psmt = conn.prepareStatement(
					"Select * From admin Where UserName = ? And NickName = ? and email = ? And pass = ?");
			psmt.setString(1, name);
			psmt.setString(2, nickname);
			psmt.setString(3, email);
			psmt.setString(4, password);
			ResultSet rs = psmt.executeQuery();
			if (rs.next())
				admin = new Admin(rs.getInt("id"), rs.getString("UserName"), rs.getString("NickName"),
						rs.getString("pass"), rs.getString("firstName"), rs.getString("lastName"),
						rs.getString("email"), rs.getInt("MobileNum"));

			psmt.close();
			// conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return admin;
	}

	public static boolean addBook(String title, String author, String genre, String publisher, String editionNum,
			String isbn, String libNum) {

		try {
			String insertBook = "INSERT INTO book VALUES(" + null + ",'" + title + "','" + author + "','" + genre
					+ "','" + publisher + "','" + editionNum + "','" + isbn + "','" + libNum + "','" + "yes' );";
			String insertJornal = "INSERT INTO journals VALUES(" + null + ",'" + title + "','" + author + "','" + genre
					+ "','" + publisher + "','" + editionNum + "','" + libNum + "','" + "yes' );";

			int res = stmt.executeUpdate(insertBook);
			stmt.executeUpdate(insertJornal);
			System.out.println(res);

			if (res > 0) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public static boolean updateBook(String title, String author, String genre, String publisher, String editionNum,
			String isbn, String libNum) {
		System.out.println(title);
		int res;
		try {

			String insertBook = "UPDATE book SET title = '" + title + "',author = '" + author + "',genre = '" + genre
					+ "', publisher = '" + publisher + "', editionNumber = '" + editionNum + "',isbn='" + isbn
					+ "'WHERE LibraryNumber = '" + libNum + " ';";

			res = stmt.executeUpdate(insertBook);
			if (res > 0) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static boolean addJornal(String title, String author, String genre, String publisher, String editionNum,
			String libNum) {

		try {
			System.out.println(" journals out");
			String insertBook = "INSERT INTO journals VALUES(" + null + ",'" + title + "','" + author + "','" + genre
					+ "','" + publisher + "','" + editionNum + "','" + libNum + "','" + "yes' );";

			int res = stmt.executeUpdate(insertBook);

			if (res > 0) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static int updateJornal(String title, String author, String genre, String publisher, String editionNum,
			String libNum) {
		System.out.println(title);
		int res;
		try {
			System.out.println("journals update out");
			String insertBook = "UPDATE journals SET title = '" + title + "',author = '" + author + "',genre = '"
					+ genre + "', publisher = '" + publisher + "', editionNumber = '" + editionNum
					+ "'WHERE LibraryNumber = '" + libNum + " ';";

			res = stmt.executeUpdate(insertBook);

			System.out.println("Result");
			System.out.println(res);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

	public String searchBook(String textField, String search_combo) {
		String Searchid = null;
		String id = null;
		String Title = null;
		String Author = null;
		String Genre = null;
		String Publisher = null;
		String EditionNumber = null;
		String ISBN = null;
		String LibraryNumber = null;
		String Stock = null;
		try {
			String resetID = " ALTER TABLE searchBook AUTO_INCREMENT=1";
			String clearPrevious = "DELETE FROM searchBook WHERE id > 0;";
			String insertSearch = "INSERT INTO searchBook (id, Title, Author, Genre, Publisher, EditionNumber, ISBN, LibraryNumber, Stock) SELECT id, Title, Author, Genre, Publisher, EditionNumber, ISBN, LibraryNumber, Stock FROM book WHERE "
					+ search_combo + " = '" + textField + "';";
			String printSearch = "SELECT Searchid, id, Title, Author, Genre, Publisher, EditionNumber, ISBN, LibraryNumber, Stock FROM searchBook WHERE searchid = 1;";

			stmt.executeUpdate(clearPrevious);
			stmt3.executeUpdate(resetID);
			stmt2.executeUpdate(insertSearch);

			rs = stmt.executeQuery(printSearch);
			while (rs.next()) {

				Searchid = rs.getString("Searchid");
				id = rs.getString("id");
				Title = rs.getString("Title");
				Author = rs.getString("Author");
				Genre = rs.getString("Genre");
				Publisher = rs.getString("Publisher");
				EditionNumber = rs.getString("EditionNumber");
				ISBN = rs.getString("ISBN");
				LibraryNumber = rs.getString("LibraryNumber");
				Stock = rs.getString("Stock");

			}
		} catch (SQLException e) {

		}
		return Searchid + "," + id + "," + Title + "," + Author + "," + Genre + "," + Publisher + "," + EditionNumber
				+ "," + ISBN + "," + LibraryNumber + "," + Stock;

	}

	public static boolean closeConn() throws SQLException {
		conn.close();
		return true;
	}

	
	
	
	public static boolean addBookRequest(String isbn, String docName, String days) {
		String name = "adam";
		System.out.println(isbn);
		System.out.println(docName);
		System.out.println(days);
		System.out.println(name);
		
		String loanBook = "INSERT INTO loaned_books VALUES('"+name+"','" +isbn+"','"+docName+"',NOW(),NOW());" ;
		
		System.out.println(loanBook);
		
		try {
			int res = stmt.executeUpdate(loanBook);
			if(res>0){
				return true;
			}
			System.out.println(res);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

		//String name = userBean.getUserName();

	}

	public static boolean closeConn() throws SQLException {
		conn.close();
		return true;
	}


	
	
	public static boolean addJournalRequest(String docName, String days) {

		String name = userBean.getUserName();
		
		try {
			String insertJournalRequest = "INSERT INTO loaned_books VALUES('title','" + docName + "', NOW(), DATE_ADD(Issue_Date,INTERVAL " + days + " DAY) );";
			

			int res = stmt.executeUpdate(insertJournalRequest);
			//stmt.executeUpdate(insertJornal);
			System.out.println(res);

			if (res > 0) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}
	
	
	
	
	public List<Overdue> getAllOverdue() throws  LibExceptionHandler, SQLException{
		// TODO Auto-generated method stub
		ArrayList<Overdue> overdueList= new ArrayList<Overdue>();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("Select userName, isbn, DocName, Issue_Date, End_Date, CURDATE()- End_Date as OverDueBy, (CURDATE()- End_Date) * 2 as fine from loaned_books Where CURDATE()- End_Date >0");
		while(rs.next()){
			String id = rs.getString("userName");
			String isbn = rs.getString("isbn");
			String DocName = rs.getString("DocName");
			String Issue_Date = rs.getString("Issue_Date");
			String End_Date = rs.getString("End_Date");
			int overdueBy=rs.getInt("OverDueBy");
			double fine=rs.getInt("fine");
			Overdue o = new Overdue(id,isbn,DocName,Issue_Date,End_Date,overdueBy,fine);
			
			overdueList.add(o);
			System.out.println(overdueList);
		}
		for(Overdue overdue : overdueList)
		{
			System.out.println("ID: "+ overdue.getStudent_id());
			System.out.println("ISBN: "+ overdue.getISBN());
			System.out.println("Title: "+ overdue.getDocName());
			System.out.println("IssueDate: "+ overdue.getIssue_Date());
			System.out.println("EndDate: "+ overdue.getEnd_Date());
		}
		return overdueList;
	}

	public List<Overdue> getAllOverdueStudent() throws SQLException {
		ArrayList<Overdue> overdueListStudent= new ArrayList<Overdue>();
		Statement stmt = conn.createStatement();
		System.out.println(StudentLoginGUI.userName);
		ResultSet rs = stmt.executeQuery("Select userName, isbn, DocName, Issue_Date, End_Date, CURDATE()- End_Date as OverDueBy, (CURDATE()- End_Date) * 2 as fine from loaned_books Where CURDATE()> End_Date And UserName = '"+StudentLoginGUI.userName+"';");
		System.out.println(rs);
		while(rs.next()){
			String id = rs.getString("userName");
			String isbn = rs.getString("isbn");
			String DocName = rs.getString("DocName");
			String Issue_Date = rs.getString("Issue_Date");
			String End_Date = rs.getString("End_Date");
			int overdueBy=rs.getInt("OverDueBy");
			double fine=rs.getInt("fine");
			Overdue o = new Overdue(id,isbn,DocName,Issue_Date,End_Date,overdueBy,fine);
			overdueListStudent.add(o);
			System.out.println(Issue_Date);
		}
		return overdueListStudent;
	}
	
	

}
