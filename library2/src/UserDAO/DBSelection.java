package UserDAO;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

import LibraryException.LibExceptionHandler;
import View.ChooseStudentOrAdminPage;

import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class DBSelection {
	public static String databaseName = null;
	private JFrame frame;
	private JTextField dbname;

	public DBSelection() {

		frame = new JFrame();
		frame.setVisible(true);
		frame.getContentPane().setBackground(SystemColor.info);
		frame.setBounds(100, 100, 626, 355);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		dbname = new JTextField();
		dbname.setHorizontalAlignment(SwingConstants.CENTER);
		dbname.setFont(new Font("Bookman Old Style", Font.ITALIC, 22));
		dbname.setText("ENTER DATABASE NAME");
		dbname.setBounds(125, 140, 363, 36);
		frame.getContentPane().add(dbname);
		dbname.setColumns(10);

		JButton btnConnect = new JButton("CONNECT");
		btnConnect.setFont(new Font("Bookman Old Style", Font.ITALIC, 20));
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String db = dbname.getText();

				Boolean bool = false;

				Connection conn2 = null;
				try {
					conn2 = DBConn.getConnection(db);
					System.out.println(conn2);
				} catch (LibExceptionHandler e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (conn2 != null) {
					bool = true;
				}

				if (bool == false) {
					JOptionPane.showMessageDialog(null, "No such database");
					dbname.setText("");
				} else {
					JOptionPane.showMessageDialog(frame, "database connected");
					ChooseStudentOrAdminPage a = new ChooseStudentOrAdminPage();
					frame.setVisible(false);
				
				}

				System.out.println(bool);
			}
		});
		btnConnect.setBounds(215, 232, 157, 36);
		frame.getContentPane().add(btnConnect);

		JLabel lblDatabaseConnection = new JLabel("D A T A B A S E  C O N N E C T I O N");
		lblDatabaseConnection.setFont(new Font("Bookman Old Style", Font.ITALIC, 30));
		lblDatabaseConnection.setBounds(31, 56, 553, 36);
		frame.getContentPane().add(lblDatabaseConnection);
	}
}
