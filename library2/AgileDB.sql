CREATE DATABASE IF NOT EXISTS AgileDB;
USE AgileDB;


CREATE TABLE IF NOT EXISTS students	(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    UserName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(30) NOT NULL,
    firstName		VARCHAR(30) NOT NULL,
    lastName		VARCHAR(30) NOT NULL,
    email			VARCHAR(30) NOT NULL,
    studentID		VARCHAR(30) NOT NULL);
    
CREATE TABLE IF NOT EXISTS admin		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	UserName		VARCHAR(30) NOT NULL,
    NickName		VARCHAR(30) NOT NULL,
    pass			VARCHAR(30) NOT NULL,
    firstName		VARCHAR(30) NOT NULL,
    lastName		VARCHAR(30) NOT NULL,
    email			VARCHAR(30) NOT NULL,
    MobileNum		INTEGER NOT NULL);

CREATE TABLE IF NOT EXISTS book		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    Title			VARCHAR(80) NOT NULL,
    Author			VARCHAR(60) NOT NULL,
    Genre			VARCHAR(60) NOT NULL,
    Publisher		VARCHAR(60) NOT NULL,
    EditionNumber	Varchar(60) NOT NULL,
    ISBN			VARCHAR(40) NOT NULL,
    LibraryNumber	VARCHAR(40) NOT NULL,
    Stock			VARCHAR(3) NOT NULL);

CREATE TABLE IF NOT EXISTS journals		(
	id				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    Title			VARCHAR(60) NOT NULL,
    Author			VARCHAR(60) NOT NULL,
    Genre			VARCHAR(60) NOT NULL,
    Publisher		VARCHAR(60) NOT NULL,
    EditionNumber	Varchar(40) NOT NULL,
    LibraryNumber	VARCHAR(40) NOT NULL,
    Stock			VARCHAR(3) NOT NULL);
    
    
CREATE TABLE IF NOT EXISTS searchBook		(
	Searchid				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    id				VARCHAR(60),
    Title			VARCHAR(60),
    Author			VARCHAR(60),
    Genre			VARCHAR(60),
    Publisher		VARCHAR(60),
    EditionNumber	Varchar(60),
    ISBN			VARCHAR(60),
    LibraryNumber	VARCHAR(60),
    Stock			VARCHAR(3));
    
    
    CREATE TABLE IF NOT EXISTS searchJournal		(
	Searchid				INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    id				VARCHAR(60),
    Title			VARCHAR(60),
    Author			VARCHAR(60),
    Genre			VARCHAR(60),
    Publisher		VARCHAR(60),
    EditionNumber	VARCHAR(60),
    LibraryNumber	VARCHAR(60),
    Stock			VARCHAR(3));

INSERT INTO book VALUES(null, 'PHP, MySQL, JavaScript & HTML5 for DUMMIES', 'Steven Suehring', 'Software', 'John Willey & Sons Inc.', '1st', '978-1-118-21370-4', '330-54', 'Yes');    
INSERT INTO book VALUES(null, 'Dynamics of WORDPERFECT', 'Dow Jones-Irwin ', 'Software', 'McGraw-Hill Professional Publishing', '1st', '978-0-870-94655-4', '331-54', 'No');    
INSERT INTO journals VALUES(null, 'PC Magazine', 'Dan Costa ', 'PC', 'Ziff Davis INC.', 'January 2017', '334-54', 'Yes');    
INSERT INTO journals VALUES(null, 'PC Magazine', 'Dan Costa', 'PC', 'Ziff Davis INC.', 'Feburary 2017','335-54', 'Yes');    
    
INSERT INTO students VALUES (null, 'oskar', 'klosowski69', 'Oskar', 'Klosowski', 'loperz@o2.pl', 'A01');
INSERT INTO students VALUES (null, 'homer21', 'simpson34', 'Homer', 'Simpson', 'loperz@o2.pl', 'A02');
INSERT INTO students VALUES (null, 'long21', 'strong18', 'Anna', 'Strong', 'loperz@o2.pl', 'A03');
INSERT INTO admin VALUES(null, 'adminX123', 'TheLegend27', 'IamAdmin23', 'Owner', 'OfTheSystem', 'loperz@o2.pl', 0878221312); 
INSERT INTO admin VALUES(null, 'aaa', 'aaa', 'aaa', 'Owner', 'OfTheSystem', 'aaa', 0878221312);    